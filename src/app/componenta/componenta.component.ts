import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'; 
import{UserService} from '../user.service';

@Component({
  selector: 'app-componenta',
  templateUrl: './componenta.component.html',
  styleUrls: ['./componenta.component.css']
})
export class ComponentaComponent implements OnInit {
fname:string="";
  lname:string="";
  fullname:string="";
  constructor(private router:Router,private UserService:UserService) { }

  ngOnInit(): void {
  }
  onSubmit()
  {
   
   
    this.fullname=this.UserService.getFullName(this.fname,this.lname);
    sessionStorage.setItem('name',this.fullname);
    this.router.navigate(['/displaydata']);
  }
}
